<?php
include "header.php";
?>

<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Invalid numbers :
      </h4>
	  
	  <style>
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div id="tab_result"></div>
	
<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
          <thead>		
		  <tr>
				<th>#</th>
				<th>Msg_Type</th>
				<th>Vou_No</th>
				<th>Msg_To</th>
				<th>Msg_To_Val</th>
				<th>Branch</th>
				<th>Branch_User</th>
				<th>Mobile</th>
				<th>Timestamp</th>
				<th>Mail_Sent</th>
				<th>#</th>
				<th>Mail_Time</th>
		</tr>
			
          </thead>	
			<tbody> 		  
            <?php
              $sql = Qry($conn,"SELECT i.id,i.vou_no,i.msg_to,i.msg_to_value,i.mobile_no,i.branch,i.msg_type,i.timestamp,
			  IF(i.mail_sent='1','YES','NO') as mail_sent,i.mail_sent as mail_sent2,i.mail_sent_timestamp,e.name as emp_name  
		 	  FROM _webhook_pinnacle_sms_invalid_number AS i 
		   	 LEFT JOIN emp_attendance AS e ON e.code=i.branch_user 
			 WHERE i.is_updated=0");
              
			if(!$sql){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
				exit();
			}
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='7'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				$timestamp = date("d/m/y h:i A",strtotime($row['timestamp']));
				
				if($row['mail_sent_timestamp']!=''){
					$mail_time = date("d/m/y h:i A",strtotime($row['mail_sent_timestamp']));
				}
				else{
					$mail_time="--";
				}
				
				if($row['mail_sent2']=='1'){
					$mail_btn_name = "Send again";
				}
				else
				{
					$mail_btn_name = "Send mail";
				}
				
				echo "<tr>
					<td>$sn</td>
					<td>$row[msg_type]</td>
					<td>$row[vou_no]</td>
					<td>$row[msg_to]</td>
					<td>$row[msg_to_value]</td>
					<td>$row[branch]</td>
					<td>$row[emp_name]</td>
					<td>$row[mobile_no]</td>
					<td>$timestamp</td>
					<td id='mail_sent_yes_no_$row[id]'>$row[mail_sent]</td>
					
					<input type='hidden' id='branch_name_$row[id]' value='$row[branch]'>
					<input type='hidden' id='vou_no_$row[id]' value='$row[vou_no]'>
					<input type='hidden' id='msg_to_$row[id]' value='$row[msg_to]'>
					<input type='hidden' id='msg_to_value_$row[id]' value='$row[msg_to_value]'>
					<input type='hidden' id='mobile_no_$row[id]' value='$row[mobile_no]'>
					<td><button type='button' id='mail_btn_$row[id]' onclick='SendMail($row[id])' class='btn btn-sm btn-danger'>$mail_btn_name</button></td>
					<td id='mail_timestamp_$row[id]'>$mail_time</td>
				</tr>";
				$sn++;		
              }
			}
            ?>
		</tbody> 	
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<div id="card_kit_data"></div>

<script>
function SendMail(id)
{
	$('#mail_btn_'+id).attr('disabled',true);
	var branch = $('#branch_name_'+id).val();
	var vou_no = $('#vou_no_'+id).val();
	var msg_to = $('#msg_to_'+id).val(); // owner/broker
	var msg_to_value = $('#msg_to_value_'+id).val(); // vehicle no/broker name
	var mobile_no = $('#mobile_no_'+id).val();
	
	$("#loadicon").show();
	jQuery.ajax({
		url: 'send_mail_invalid_no.php',
		data: 'id=' + id + '&branch=' + branch + '&vou_no=' + vou_no + '&msg_to=' + msg_to + '&msg_to_value=' + msg_to_value + '&mobile_no=' + mobile_no,
		type: "POST",
		success: function(data){
			$("#card_kit_data").html(data);
		},
		error: function() {}
	});
}

$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>