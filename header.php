<?php
require_once("./connect.php");
$menu_page_name = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
	<meta name="robots" content="noindex,nofollow"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>RRPL : Message admin</title>
	<link rel="shortcut icon" type="image/png" href="../favicon.png"/>
	<link rel="shortcut icon" type="image/png" href="./favicon.png"/>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
  
  <!-- Morris chart -->
  <link rel="stylesheet" href="../happay/bower_components/morris.js/morris.css">
  <link rel="stylesheet" href="../happay/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="../happay/bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="../happay/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../happay/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="../happay/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="../happay/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="../happay/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
<link href="../happay/google_font.css" rel="stylesheet">

<style>
@media screen and (min-width: 769px) {

    #logo_mobile { display: none; }
    #logo_desktop { display: block; }

}

@media screen and (max-width: 768px) {

    #logo_mobile { display: block; }
    #logo_desktop { display: none; }

}
</style>

</head>

<style type="text/css">
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
  z-index: 9999;
 /* background: url(load_icon.gif) center no-repeat #fff; */
  background: url(loading_truck.gif) center no-repeat #fff;
}
</style>

<script type="text/javascript">
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");;
  });
</script>

<body style="font-family: 'Open Sans', sans-serif !important" onkeypress="return disableCtrlKeyCombination(event);" onkeydown = "return disableCtrlKeyCombination(event);" class="hold-transition skin-blue sidebar-mini">
 
 <div class="se-pre-con"></div>
<div class="wrapper">

  <header class="main-header" style="font-size:12px;">

	<a href="./" class="logo" style="background:#FFF">
      <span class="logo-mini"><img src="logo_small_rrpl.png" style="width:100%;height:50px" class="" /></span>
      <span class="logo-lg" id="logo_desktop"><img src="logo_raman_roadways.png" style="margin-top:5px;width:100%;height:40px" class="img-responsive" /></span>
      <span class="logo-lg" id="logo_mobile"><center><img src="logo_raman_roadways.png" style="margin-top:5px;width:50%;height:40px" class="img-responsive" /></center></span>
    </a>

    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
		
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../happay/avtar2.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo  "Message Admin"; ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="../happay/avtar2.png" class="img-circle" alt="User Image">
				<p>
                   <?php echo "Message Admin"; ?>
                </p>
              </li>
              <!-- Menu Body -->
            
              <!-- Menu Footer-->
              <li class="user-footer">
          
                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Log out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!--
		  <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
        </ul>
      </div>

    </nav>
  </header>
  
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
       
      </div>
      
	  <ul style="font-size:13px;" class="sidebar-menu" data-widget="tree">          
             
 	<li class="<?php if($menu_page_name=="index.php") {echo "active";} ?>">
        <a href="./index.php"><i class="fa fa-envelope-open-o"></i> <span>Pending delivery</span></a>
     </li>
	 
	
	 <li class="<?php if($menu_page_name=="msg_failed.php") {echo "active";} ?>">
        <a href="./msg_failed.php"><i class="fa fa-exclamation-triangle"></i> <span>Delivery failure</span></a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="invalid_numbers.php") {echo "active";} ?>">
        <a href="./invalid_numbers.php"><i class="fa fa-window-close"></i> <span>Invalid numbers</span></a>
     </li>
	 
	 <li class="<?php if($menu_page_name=="emp_details_pending.php") {echo "active";} ?>">
        <a href="./emp_details_pending.php"><i class="fa fa-window-close"></i> <span>Emp. details pending</span></a>
     </li>
	 
	 <li>
		<a href="logout.php"><i class="glyphicon glyphicon-log-out"></i> <span>Logout</span></a>
     </li>
		
      </ul>
    </section>
  </aside>
  
  