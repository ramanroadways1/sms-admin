<?php
require_once("./connect.php");
require($_SERVER['DOCUMENT_ROOT']."/graph/smtp/PHPMailerAutoload.php");

$timestamp = date("Y-m-d H:i:s");

$id = escapeString($conn,($_POST['id']));
$branch = escapeString($conn,($_POST['branch']));
$vou_no = escapeString($conn,($_POST['vou_no']));
$msg_to = escapeString($conn,($_POST['msg_to']));
$msg_to_value = escapeString($conn,($_POST['msg_to_value']));
$mobile_no = escapeString($conn,($_POST['mobile_no']));

$get_mail_id = Qry($conn,"SELECT email FROM user WHERE username='$branch'");

if(!$get_mail_id){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

if(numRows($get_mail_id)==0)
{
	echo "<script>
		alert('Email id not found..');
		$('#mail_btn_$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

if(strpos($msg_to,'BROKER') !== false)
{
    $msg_to='BROKER';
}
else
{
	$msg_to="OWNER";
}

$row_mail = fetchArray($get_mail_id);
$email = $row_mail['email'];

$message = '<html><body style="font-family:Verdana">';
$message .= '<span style="color:maroon;font-size:13px"><b>Please update valid mobile number for the Owner/Broker details mentioned below: </b></span>';
$message .= '<br><br>
<b>Voucher number: </b>'.$vou_no.'<br><br>
<b>Advance to: </b>'.$msg_to.'<br><br>
<b>Party name / Vehicle number: </b>'.$msg_to_value.'<br><br>
<b>Mobile number: </b><font color="red">'.$mobile_no.'</font><br>
<br>
To update mobile number login into <b>"UMMEED"</b> using your username and password.
<br>
<br>
>> Click on Function.
>> Click on Update Owner/Broker Mobile Number.
';
$message .= "</body></html>";

if($email=='')
{
	echo "<script>
		alert('Email id: $email is not valid..');
		$('#mail_btn_$id').attr('disabled',false);
		$('#loadicon').fadeOut('slow');
	</script>";
	exit();
}

	$username = 'noreply@ramanroadways.com';                   
	$password = 'rrpl123#';
	// $to_id = "system@ramanroadways.com";
	$to_id = $email;
	$subject = 'Invalid Mobile No. : Urgent action required';
	$mail = new PHPMailer;
    $mail->isSMTP();
	$mail->Host = 'smtp.rediffmailpro.com';
	$mail->Port = 587;
	$mail->SMTPSecure = 'tls';
	$mail->SMTPAuth = true;
	$mail->Username = $username;
	$mail->Password = $password;
	$mail->setFrom('noreply@ramanroadways.com', 'Raman Roadways');
	// $mail->addReplyTo('accounts@ramanroadways', 'Accounts RamanRoadways');
	$mail->addAddress($to_id);
	$mail->AddCC('data@ramanroadways.com', 'RamanRoadways Data-Center');
	// $mail->AddCC('system@ramanroadways.com', 'RRPL SYSTEM ADMIN');
	$mail->Subject = $subject;
	$mail->msgHTML($message);
	// $mail->AddAttachment("./_excel_autosave/Tyre_Exp.xlsx");
		
	if(!$mail->send())
	{
		$mail_error = "Mailer Error: " . $mail->ErrorInfo;
		errorLog($mail_error,$conn,$page_name,__LINE__);
		echo "<script>
			alert('Error while sending email..');
			$('#mail_btn_$id').attr('disabled',false);
			$('#loadicon').fadeOut('slow');
		</script>";
		exit();
   } 
	
$update_mail_sent = Qry($conn,"UPDATE _webhook_pinnacle_sms_invalid_number SET mail_sent='1',mail_sent_timestamp='$timestamp' 
WHERE id='$id'");

if(!$update_mail_sent){
	errorLog(getMySQLError($conn),$conn,$page_name,__LINE__);
	Redirect("Error while processing Request","./");
	exit();
}

	echo "<script>
			// alert('Mail sent !!');
			$('#mail_btn_$id').attr('disabled',true);
			$('#mail_btn_$id').html('Mail Sent !');
			$('#mail_btn_$id').attr('class','btn btn-sm btn-success');
			$('#mail_timestamp_$id').html('$timestamp');
			$('#mail_sent_yes_no_$id').html('YES');
			$('#loadicon').fadeOut('slow');
	</script>";
	exit();
?>