<?php
include "header.php";
?>

<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Msg failed to deliver :
      </h4>
	  
	  <style>
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div id="tab_result"></div>
	
<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="example" class="table table-bordered table-striped" style="font-size:12.5px;">
          <thead>		
		  <tr>
				<th>#</th>
				<th>SenderID</th>
				<th>Msg_Type</th>
				<th>Vou_No</th>
				<th>Role/Branch</th>
				<th>Username</th>
				<th>Mobile</th>
				<th>Status_Code</th>
				<th>Delivery_Status</th>
				<th>Timestamp</th>
		</tr>
			
          </thead>	
			<tbody> 		  
            <?php
              $sql = Qry($conn,"SELECT e.del_status_code,e.del_status,e.smsstatus,s.sender_id,s.msg_type,s.vou_no,
			  s.role_type,s.session_role_code,s.mobile,s.timestamp,u.username,emp.name as emp_name,emp.branch as emp_branch  
			  FROM _webhook_pinnacle_sms_error AS e 
			  LEFT JOIN _webhook_pinnacle_sms AS s ON s.id=e.webhook_id
			  LEFT JOIN emp_attendance AS emp ON emp.code=s.session_role_code
		     LEFT JOIN user AS u ON u.role=s.session_role_code AND u.mobile_no=s.mobile");
              
			if(!$sql){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
				exit();
			}
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='7'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				$timestamp = date("d/m/y h:i A",strtotime($row['timestamp']));
				
				$sms_status=$row['smsstatus'];
				
				if($row['smsstatus']=='NA'){
					$sms_status=$row['del_status'];
				}
				
				if($row['role_type']=='Branch'){
					 $role_type=$row['emp_branch'];
					 $username=$row['emp_name'];
				 }
				 else if($row['role_type']=='Others'){
					 $role_type="Others";
					 $username=$row['username'];
				 }
				 else{
					 $role_type="NA";
					 $username="NA";
				 }
				
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[sender_id]</td>
					<td>$row[msg_type]</td>
					<td>$row[vou_no]</td>
					<td>$role_type</td>
					<td>$username</td>
					<td>$row[mobile]</td>
					<td>$row[del_status_code]</td>
					<td>$sms_status</td>
					<td>$timestamp</td>
				</tr>";
				$sn++;		
              }
			}
            ?>
		</tbody> 	
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<div id="card_kit_data"></div>

<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>