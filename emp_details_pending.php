<?php
include "header.php";
?>

<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Employee details pending :
      </h4>
	  
	  <style>
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div id="tab_result"></div>
	
<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="example" class="table table-bordered table-striped" style="font-size:12px;">
          <thead>		
		  <tr>
				<th>#</th>
				<th>Name</th>
				<th>Code</th>
				<th>Branch</th>
				<th>Mobile</th>
				<th>Update Timestamp</th>
		</tr>
			
          </thead>	
			<tbody> 		  
            <?php
              $sql = Qry($conn,"SELECT * FROM emp_attendance_pending");
              
			if(!$sql){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
				exit();
			}
			  
			  if(numRows($sql)==0)
			  {
				echo "<tr>
						<td colspan='6'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			  }
			 else
			 {
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				if($row['timestamp_updated']!=''){
					$timestamp = date("d/m/y h:i A",strtotime($row['timestamp_updated']));
				}
				else{
					$timestamp="";
				}
				
			echo "<tr>
					<td>$sn</td>
					<td>$row[name]</td>
					<td>$row[code]</td>
					<td>$row[branch]</td>
					<td>$row[mobile]</td>
					<td>$timestamp</td>
				</tr>";
				$sn++;		
              }
			}
            ?>
		</tbody> 	
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<div id="card_kit_data"></div>

<script>
// function SendMail(id)
// {
	// $('#mail_btn_'+id).attr('disabled',true);
	// var branch = $('#branch_name_'+id).val();
	// var vou_no = $('#vou_no_'+id).val();
	// var msg_to = $('#msg_to_'+id).val(); // owner/broker
	// var msg_to_value = $('#msg_to_value_'+id).val(); // vehicle no/broker name
	// var mobile_no = $('#mobile_no_'+id).val();
	
	// $("#loadicon").show();
	// jQuery.ajax({
		// url: 'send_mail_invalid_no.php',
		// data: 'id=' + id + '&branch=' + branch + '&vou_no=' + vou_no + '&msg_to=' + msg_to + '&msg_to_value=' + msg_to_value + '&mobile_no=' + mobile_no,
		// type: "POST",
		// success: function(data){
			// $("#card_kit_data").html(data);
		// },
		// error: function() {}
	// });
// }

$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>