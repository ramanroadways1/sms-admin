<?php
include "header.php";
?>

<div class="content-wrapper">
    <section class="content-header">
      <h4>
		Msg pending delivery :
      </h4>
	  
	  <style>
	  .form-control{text-transform:uppercase;}
	  </style>
	  
<div id="loadicon" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity: 0.9;">
	<center><img style="margin-top:150px" src="./loader.gif" /></center>
</div>		  
	  
    </section>
    <section class="content">
       <div class="row">
            <div class="col-md-12">
            <div class="box"> 
			<div class="box-body">

<div id="tab_result"></div>
	
<div class="row">
 
 <div class="form-group col-md-12">
    
	<div class="form-group col-md-12 table-responsive">
	 <br />
        <table id="example" class="table table-bordered table-striped" style="font-size:12.5px;">
          <thead>		
		  <tr>
				<th>#</th>
				<th>SenderID</th>
				<th>Msg_Type</th>
				<th>Vou_No</th>
				<th>Role/Branch</th>
				<th>Username</th>
				<th>Mobile</th>
				<th>Delivery_Status</th>
				<th>Timestamp</th>
		</tr>
			
          </thead>	
			<tbody> 		  
            <?php
            $sql = Qry($conn,"SELECT p.del_status,s.sender_id,s.vou_no,s.role_type,s.session_role_code,s.msg_type,
			s.mobile,s.timestamp,u.username,e.name as emp_name,e.branch as emp_branch 
			FROM _webhook_pinnacle_sms_pending AS p 
			LEFT JOIN _webhook_pinnacle_sms AS s ON s.id=p.webhook_id
			LEFT JOIN emp_attendance AS e ON e.code=s.session_role_code
			LEFT JOIN user AS u ON u.role=s.session_role_code AND u.mobile_no=s.mobile");
              
			if(!$sql){
				echo getMySQLError($conn);
				errorLog(getMySQLError($conn),$conn,$page_url,__LINE__);
				exit();
			}
			  
			if(numRows($sql)==0)
			{
				echo "<tr>
						<td colspan='6'><b>NO RESULT FOUND..</b></td>
					</tr>";  
			}
			else
			{
			  $sn=1;
			  
			  while($row = fetchArray($sql))
			  {
				 if($row['role_type']=='Branch'){
					 $role_type=$row['emp_branch'];
					 $username=$row['emp_name'];
				 }
				 else if($row['role_type']=='Others'){
					 $role_type="Others";
					 $username=$row['username'];
				 }
				 else{
					 $role_type="NA";
					 $username="NA";
				 }
				 
				$timestamp = date("d/m/y h:i A",strtotime($row['timestamp']));
				  
				echo 
                "<tr>
					<td>$sn</td>
					<td>$row[sender_id]</td>
					<td>$row[msg_type]</td>
					<td>$row[vou_no]</td>
					<td>$role_type</td>
					<td>$username</td>
					<td>$row[mobile]</td>
					<td>$row[del_status]</td>
					<td>$timestamp</td>
				</tr>";
				$sn++;		
              }
			}
            ?>
		</tbody> 	
        </table>
      </div>
    </div>
  </div>
</div>

</body>
</html>
            </div>
          </div>
		  </div>
       </div>         
    </section>

<div id="card_kit_data"></div>

<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php
include "footer.php";
?>