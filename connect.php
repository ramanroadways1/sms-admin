<?php
session_start();

include($_SERVER['DOCUMENT_ROOT']."/_connect.php"); // for localhost

date_default_timezone_set('Asia/Kolkata');
$timestamp=date("Y-m-d H:i:s");

$page_name=$_SERVER['REQUEST_URI'];
$page_url=$_SERVER['REQUEST_URI'];

if(!isset($_SESSION['_sms_admin']))
{
	echo "<script>
		window.location.href='./logout.php';
	</script>";
	exit();
}
?>